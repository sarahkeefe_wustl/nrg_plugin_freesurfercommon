package org.nrg.xnat.freesurfercommon;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_freesurfercommon", name = "XNAT 1.7 FreeSurfer Common Plugin", description = "This is the XNAT 1.7 FreeSurfer Common Plugin.",
        dataModels = {@XnatDataModel(value = "fs:asegRegionAnalysis",
                singular = "ASEG",
                plural = "ASEGs"),
                @XnatDataModel(value = "fs:aparcRegionAnalysis",
                        singular = "APARC",
                        plural = "APARCs"),
                @XnatDataModel(value = "fs:automaticSegmentationData",
                        singular = "Auto Seg",
                        plural = "Auto Segs"),
                @XnatDataModel(value = "fs:fsData",
                        singular = "Freesurfer",
                        plural = "Freesurfers"),
                @XnatDataModel(value = "fs:longFSData",
                        singular = "LongitudinalFS",
                        plural = "LongitudinalFSs")})
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class FreesurferCommonPlugin {
}